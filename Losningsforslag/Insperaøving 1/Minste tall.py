min_tall = float('inf')
for i in range(5):
    tall = float(input("Skriv et tall: "))
    if (tall < min_tall):
        min_tall = tall
print(f'Det minste tallet du skrev inn var {min_tall}.')
