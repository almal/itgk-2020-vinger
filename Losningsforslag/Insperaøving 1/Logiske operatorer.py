# Del 1
month = input("Skriv inn en måned: ")
year = int(input("Skriv inn et år: "))
month = month.lower()

if (month == "januar") or (month == "mars") or (month == "mai") or (month == "juli") or (month == "august") or (month == "oktober") or (month == "desember"):
    print(31)
elif (month == "april") or (month == "juni") or (month == "september") or (month == "november"):
    print(30)
elif month == "februar":
    if year%4 == 0:
        print(29)
    else:
        print(28)
        
# Del 2
valid = False

while not valid:

    month = input("Skriv inn en måned: ")
    year = int(input("Skriv inn et år: "))
    month = month.lower()
    valid_year = ((year >= 0) and (year <= 2020))
    
    if not valid_year: # ugyldig år
        print("Ugyldig input! Prøv på nytt!")
        continue

    if ((month == "januar") or (month == "mars") or (month == "mai") or (month == "juli") or (month == "august") or (month == "oktober") or (month == "desember")):
        number_of_days = 31
        valid = True
    elif ((month == "april") or (month == "juni") or (month == "september") or (month == "november")):
        number_of_days = 30
        valid = True
    elif month == "februar":
        if year % 4 == 0:
            number_of_days = 29
        else:
            number_of_days = 28
        valid = True
    else: #ugyldig måned
        print("Ugyldig input! Prøv på nytt!")

print("Det er",number_of_days ,"dager i denne måneden.")
