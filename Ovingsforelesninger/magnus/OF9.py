def sekvensielt_sok(liste, tall):
    for element in liste:
        if tall == element:
            return True
    return False

#liste = [3, 5, 7, 2332, 6655, 77, 4, 2, 1]
#print(sekvensielt_sok(liste, 7))


# Oppg2
def binaer_sok(liste, tall, min, max):

    if min > max:
        return False

    mid = (max + min) // 2

    if liste[mid] == tall:
        return True

    if tall < liste[mid]:
        return binaer_sok(liste, tall, min, mid-1)
    else:
        return binaer_sok(liste, tall, mid+1, max)

#arr = [3, 5, 20, 44, 56, 75, 77, 100]
#print(binaer_sok(arr, 76, 0, len(arr)-1))

def get_prioritized_list(liste):
    nliste = list(liste)

    for i in range(1, len(nliste)):
        curVal = nliste[i]
        curPos = i
        while curPos > 0 and nliste[curPos - 1][1] < curVal[1]:
            nliste[curPos] = nliste[curPos-1]
            curPos -= 1
        nliste[curPos] = curVal
        while curPos > 0 and nliste[curPos - 1][1] == curVal[1] and nliste[curPos - 1][0] > curVal[0]:
            nliste[curPos] = nliste[curPos - 1]
            curPos -= 1
        nliste[curPos] = curVal
    return nliste

#print(get_prioritized_list([ ("Pancakes", 10), ("Dogs", 10), ("Cats", 10), ("Ice cream", 9), ("Chocolate", 8)]))

def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)

print(factorial(5))

def binomial(n, k):
    return factorial(n) / (factorial(k)*factorial(n-k))

#print(binomial(10, 4))

def sum_list(liste):
    sum = 0
    for elem in liste:
        if isinstance(elem, list):
            sum += sum_list(elem)
        else:
            sum += elem
    return sum

print(sum_list([[4,5,1], 5, 2, 8, [2], [21, [9123,[23] ,3], 2]]))
